package com.spring.basics.springin5steps;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.spring.basics.springin5steps.basic.BinarySearchImp;
import com.spring.basics.springin5steps.properties.SomeExternalService;

@Configuration
@ComponentScan
@PropertySource("classpath:app.properties")
public class PropertiesApplication {

	public static void main(String[] args) {				
		try(ConfigurableApplicationContext applicationContext = new AnnotationConfigApplicationContext(PropertiesApplication.class)) {
			SomeExternalService service  = applicationContext.getBean(SomeExternalService.class);	
			
			System.out.println(service.returnServiceURL());			
		}
	}
}
