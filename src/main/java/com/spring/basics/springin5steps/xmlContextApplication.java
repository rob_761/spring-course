package com.spring.basics.springin5steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.basics.springin5steps.xml.XmlPersonDAO;

public class xmlContextApplication {

	private static Logger LOGGER = LoggerFactory.getLogger(ScopeApplication.class);

	
	public static void main(String[] args) {				
		try(ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml")) {
			
			LOGGER.info("Beans Loaded -> {}", (Object)applicationContext.getBeanDefinitionNames());
			
			XmlPersonDAO personDao  = applicationContext.getBean(XmlPersonDAO.class);	
			
			System.out.println(personDao);			
			System.out.println(personDao.getXmlJdbcConnection());			
		}
	}
}
