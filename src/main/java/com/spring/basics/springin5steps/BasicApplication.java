package com.spring.basics.springin5steps;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.spring.basics.springin5steps.basic.BinarySearchImp;

@Configuration
@ComponentScan
public class BasicApplication {

	public static void main(String[] args) {				
		try(ConfigurableApplicationContext applicationContext = new AnnotationConfigApplicationContext(BasicApplication.class)) {
			BinarySearchImp binarySearch  = applicationContext.getBean(BinarySearchImp.class);	
			
			int result = binarySearch.binarySearch(new int[] {12, 4, 6}, 3);
			System.out.println(result);			
		}
	}
}
