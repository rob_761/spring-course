package com.spring.basics.springin5steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.spring.basics.componentscan.ComponentDAO;

@Configuration
@ComponentScan("com.spring.basics.componentscan")
public class ScopeApplication {

	private static Logger LOGGER = LoggerFactory.getLogger(ScopeApplication.class);
	
	public static void main(String[] args) {
		try(ConfigurableApplicationContext applicationContext = new AnnotationConfigApplicationContext(ScopeApplication.class)) {
			
//			PersonDAO personDao = applicationContext.getBean(PersonDAO.class);
//			PersonDAO personDao2 = applicationContext.getBean(PersonDAO.class);
			
			ComponentDAO componentDao = applicationContext.getBean(ComponentDAO.class);
			ComponentDAO componentDao2 = applicationContext.getBean(ComponentDAO.class);
			
//			LOGGER.info("{}", personDao);
//			LOGGER.info("{}", personDao.getJdbcConnection());
	
//			LOGGER.info("{}", personDao2);
//			LOGGER.info("{}", personDao2.getJdbcConnection());
			
			LOGGER.info("{}", componentDao);
			LOGGER.info("{}", componentDao.getJdbcConnection());

			LOGGER.info("{}", componentDao2);
			LOGGER.info("{}", componentDao2.getJdbcConnection());
		}
	}
}
