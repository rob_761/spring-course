package com.spring.basics.springin5steps.cdi;

import javax.inject.Named;

@Named
public class SomeCDIDAO {

	public int[] getData() {
		return new int[] {1, 20, 30};
	}
}
