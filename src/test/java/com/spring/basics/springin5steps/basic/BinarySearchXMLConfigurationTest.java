package com.spring.basics.springin5steps.basic;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

//Load spring context
@RunWith(SpringRunner.class)
@ContextConfiguration(locations="/testContext.xml")
public class BinarySearchXMLConfigurationTest {
	
	//get bean from context
	@Autowired
	BinarySearchImp binarySearch;

	@Test
	public void testBasicScenario() {
		int result = binarySearch.binarySearch(new int[]{1,2,4,5}, 5);
		assertEquals(3,result);
	}

}
