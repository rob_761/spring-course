package com.spring.basics.springin5steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BasicApplication.class)
public class SpringIn5StepsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
